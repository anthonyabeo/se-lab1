public class Calc{
	
	public static void main(String[]args){
		
		if(args.length >= 3){
			String command = args[0];
			
			if(command.equalsIgnoreCase("add")){
				double sum = 0;
				for(int i = 1; i < args.length; i++){
					sum += Float.valueOf(args[i]);
				}
				System.out.println("SUM: " + sum);
			}
			else if(command.equalsIgnoreCase("subtract")){
				double diff = Float.valueOf(args[1]) - Float.valueOf(args[2]);
				System.out.println("DIFFERENCE: " + diff);
			}
			else if(command.equalsIgnoreCase("multiply")){
				double product = Float.valueOf(args[1]) * Float.valueOf(args[2]);
				System.out.println("PRODUCT: " + product);
			}
			else if (command.equalsIgnoreCase("divide")){
				double quotient = Float.valueOf(args[1]) / Float.valueOf(args[2]);
				System.out.println("QUOTIENT " + quotient);
			}
		}
		else
			System.out.println("Insufficient number of arguments");
	
	}		
}
